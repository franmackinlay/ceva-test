// Exercice: MongoDb aggregate (5 points)

// MongoDb collection users with schema

//   {
//     email: string;
//     first_name: string;
//     last_name: string;
//     roles: string[];
//     last_connection_date: Date;
//   }


// Complete the aggregation so that it sends user emails by role ({_id: 'role', users: [email,...]})

// ! Disclamer: I've never used the MongoDb Aggregation Framework before, so I had to look it up and try to solve it using external resources
// ! like StackOverflow, the oficial docs, etc.

db.collections('users').aggregate([
  {
    $unwind: '$roles',
  },
  {
    $group: {
      _id: '$roles',
      users: {
        $push: '$email',
      }
    }
  },
  {
    $project: {
      role: '$_id',
      users: 1,
    }
  }
])


