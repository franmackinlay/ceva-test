// {
//   email: string;
//   first_name: string;
//   last_name: string;
//   roles: string[];
//   last_connection_date: Date;
// }

const searchText = "variable that contains a piece of text to search for";
const sixMonthsAgo = new Date();
sixMonthsAgo.setMonth(sixMonthsAgo.getMonth() - 6);

db.users.find({
  $or: [
    { email: searchText },
    { first_name: { $regex: `^${searchText}` } },
    { last_name: { $regex: `^${searchText}` } }
  ],
  last_connection_date: { $gte: sixMonthsAgo }
});


// ? What should be added to the collection so that the query is not slow?

// * MongoDb performs faster when indexes are created and added to the MongoDb collection.
