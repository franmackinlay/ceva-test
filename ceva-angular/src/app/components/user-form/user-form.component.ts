import { CommonModule } from '@angular/common';
import { Component, Output, EventEmitter } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';

interface FormControlConfig {
  type: string;
  name: string;
  placeholder: string;
  validators?: any[];
}

interface Address {
  zip: string;
  city: string
}

interface FormResponse {
  email: string;
  name: string;
  birthday?: string;
  address: Address
}

@Component({
  selector: 'app-user-form',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule],
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export default class AppUserForm {

  @Output() event = new EventEmitter<FormResponse>();

  userForm: FormGroup;

  formControls: FormControlConfig[] = [
    { type: 'email', name: 'email', placeholder: 'Email', validators: [Validators.required, Validators.email] },
    { type: 'text', name: 'name', placeholder: 'Name', validators: [Validators.required, Validators.maxLength(128)] },
    { type: 'number', name: 'zip', placeholder: 'Zip', validators: [Validators.required] },
    { type: 'text', name: 'city', placeholder: 'City', validators: [Validators.required, this.validCharsOnly] },
    { type: 'date', name: 'birthday', placeholder: 'Birthday', validators: [] },
  ];

  constructor(private formBuilder: FormBuilder) {
    this.userForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      name: ['', [Validators.required, Validators.maxLength(128)]],
      birthday: ['', this.birthdayValidator],
      zip: ['', Validators.required],
      city: ['', [Validators.required, Validators.pattern(/^[a-zA-Z ]*$/)]],
    });
  }

  doSubmit(): void {
    if (!this.userForm.valid) {
      alert('Form invalid, please check the form fields')
      return
    }

    const formResponse: FormResponse = {
      email: this.userForm.value.email,
      name: this.userForm.value.name,
      address: {
        zip: this.userForm.value.zip,
        city: this.userForm.value.city,
      },
    }

    if (this.userForm.value.birthday)
      formResponse.birthday = this.userForm.value.birthday

    this.event.emit(formResponse);
  }

  private birthdayValidator(control: FormControl): { [key: string]: boolean } | null {
    const birthday = new Date(control.value);
    const today = new Date();
    today.setHours(0, 0, 0, 0);

    return birthday >= today ? { dateInvalid: true } : null;
  }

  private validCharsOnly(control: AbstractControl): { [key: string]: boolean } | null {
    const pattern = /^[a-zA-Z ]*$/;
    return pattern.test(control.value) ? null : { invalidAlphaSpace: true };
  }
}
