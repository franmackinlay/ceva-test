import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-users',
  template: `
    <div *ngFor="let user of users">
        {{ getCapitalizeFirstWord(user.name) }}
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush // TODO: added changeDetection to execute only when a change is detected
})
export class AppUsers {

  @Input()
  users: { name: string }[];

  getCapitalizeFirstWord(name: string): string {
    return name
      .split(' ')
      .map(n => n.substring(0, 1).toUpperCase() + n.substring(1).toLowerCase())
      .join(' ');
  }
}
