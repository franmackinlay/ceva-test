//Exercice: Is there a problem and improve the code (5 points)

@Component({
  selector: 'app-users',
  template: `
    <input type="text" [(ngModel)]="query" (ngModelChange)="onQueryChange()">
    <div *ngFor="let user of users">
        {{ user.email }}
    </div>
  `
})
export class AppUsers implements OnInit, OnDestroy {

  query = '';
  querySubject = new Subject<string>();
  users: { email: string }[] = [];
  private destroy$ = new Subject<void>();

  constructor(private userService: UserService) {}

  ngOnInit(): void {
    concat(
      of(this.query),
      this.querySubject.asObservable()
    ).pipe(
      debounceTime(1000),                   // TODO: Add debouceTime to reduce api calls
      takeUntil(this.destroy$),             // TODO: unsubscribe from observables to prevent memory leaks.
      concatMap(q =>
        timer(0, 60000).pipe(
          this.userService.findUsers(q),
          catchError(error => {             // TODO: Added error handling
            console.error('Error fetching users:', error);
            return of([]);
          })
        )
      )
    ).subscribe({
      next: (res) => this.users = res
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  onQueryChange(): void {
    this.querySubject.next(this.query);
  }
}

