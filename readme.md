# Ceva Test

I've divided the different sections of this test into it's corresponding folders.

* Angular/Javascript/MongoDb/Test: Contains the 1-3 corresponding exercises. I pretended that they were corrections made in a PR, commenting* on the error and proposing a solution below (case Angular and Javascript). There is a little disclamer in ex-2 about MongoDb, hope it's ok.

    (* install the vscode Better Comments extension to see the comments better if possible)

* ceva-angular: contains the exercises from Angular Forms/CSS & Bootstrap. To see the results you must follow the steps below:

## Software requirements

* Node v18
* Angular CLI
* Typescript

### From root directory

```
  cd ceva-angular
```

## Install dependencies

```
npm install
```

## Run in local

```
npm start
```
