// Write unit tests in jest for the function below in typescript

import { expect, test } from '@jest/globals';

const CONSTANTS = {
  SENTENCE: {
    LOWERCASE: 'write unit tests in jest for the function below in typescript',
    UPPERCASE: 'Write Unit Tests In Jest For The Function Below In Typescript',
  },
  EMPTY_STRING: ''
}

function getCapitalizeFirstWord(name: string): string {
  // ! This check seems redudant, as name is styped a string, it should never allow it to be null from scratch.
  // ! Also, why == instead of === ?
  if (name == null) {
    throw new Error('Failed to capitalize first word with null');
  }
  if (!name) {
    return name;
  }
  return name.split(' ').map(
    n => n.length > 1 ? (n.substring(0, 1).toUpperCase() + n.substring(1).toLowerCase()) : n
  ).join(' ');
}

test('Should capitalize the first letter of each word', async function () {
  const result = getCapitalizeFirstWord(CONSTANTS.SENTENCE.LOWERCASE);
  expect(result).toBe(CONSTANTS.SENTENCE.UPPERCASE);
});

test('empty string input', async function () {
  const result = getCapitalizeFirstWord(CONSTANTS.EMPTY_STRING);
  expect(result).toBe(CONSTANTS.EMPTY_STRING);
});
